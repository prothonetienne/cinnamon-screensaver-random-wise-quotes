# Cinnamon ScreenSaver Random Wise Quote

## Set up
1. Install qdbus `sudo apt-get install qdbus`
1. Clone the repository
1. Open your keyboard settings
1. Select the "Shortcuts" tab
1. Choose "Add custom shortcut"
1. Give a name *(ex. "Wise ScreenSaver")* and select the file screensaver from the repository you just clone
1. Add Keyboard bindings (if you want to overwrite the current screensaver key bind you must add Ctrl-Alt-L)

## Add your own quotes
You can use line break with `\n` and if you know the author of your quote please add it at the end *(ex. "Wise quote" - Author)*
